
public class App {

	public static void main(String[] args) {
		// with of int = 32 (4 bytes)
		int myIntValue = 5/3;
		// width of float = 32 (4 bytes)
		float myFloatValue = 5F/2F;
		// width of double = 64 (8 bytes)
		double myDoubleValue = 5D/2D;
		System.out.println(myIntValue);
		System.out.println(myFloatValue);
		System.out.println(myDoubleValue);
		
		
		double pounds = 200D;
		double kilograms = pounds * 0.45359237D;
		System.out.println(kilograms);
		
		double pi = 3.1415927D;
		
		
	}

}
